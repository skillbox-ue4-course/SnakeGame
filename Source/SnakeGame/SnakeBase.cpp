// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	MovementSpeed = 10.0f; // in sec
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	
	SetSankeDefaultLenght();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

	if (SnakeElements.Num() > 15) {
		MovementSpeed = 0.05f;;
		SetActorTickInterval(MovementSpeed);
	}
	else if (SnakeElements.Num() > 10) {
		MovementSpeed = 0.15f;;
		SetActorTickInterval(MovementSpeed);
	}
	else if (SnakeElements.Num() > 6) {
		MovementSpeed = 0.3f;;
		SetActorTickInterval(MovementSpeed);
	}
	else if (SnakeElements.Num() < 5) {
		MovementSpeed = 0.5f;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::SetSankeDefaultLenght()
{
	if (SnakeElements.Num() == 0) {
		// spawn head
		AddSankeHead();
		// add some elems
		AddSankeElement(6);
	}
	else {
		
		while (SnakeElements.Num() > 4)
		{
			SnakeElements[SnakeElements.Num() - 1]->Destroy(false);
			SnakeElements.RemoveAt(SnakeElements.Num() - 1);
		}
	}
}

void ASnakeBase::AddSankeHead()
{
	// spawn head at start
	FVector NewLocation = FVector(0, 0, ElementSize);
	FTransform NewTransform(NewLocation);
	ASnakeElementBase* NewSankeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	NewSankeElem->SnakeOwner = this;

	NewSankeElem->SetFirstElemType();

	SnakeElements.Add(NewSankeElem);

	NewSankeElem->ElementDirectionNow        = LastMoveDirection;
	NewSankeElem->ElementDirectionStepBefore = LastMoveDirection;
}

void ASnakeBase::AddSankeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; i++) {

		// calculate new location
		auto PrevElem = SnakeElements[SnakeElements.Num() - 1];
		FVector NewLocation = PrevElem->GetActorLocation();

		switch (PrevElem->ElementDirectionStepBefore)
		{
		case EMovementDirection::UP:
			NewLocation.X -= ElementSize;
			break;
		case EMovementDirection::DOWN:
			NewLocation.X += ElementSize;
			break;
		case EMovementDirection::LEFT:
			NewLocation.Y -= ElementSize;
			break;
		case EMovementDirection::RIGHT:
			NewLocation.Y += ElementSize;
			break;
		default:
			break;
		}
		

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSankeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSankeElem->SnakeOwner = this;

		NewSankeElem->ElementDirectionStepBefore = PrevElem->ElementDirectionStepBefore;
		NewSankeElem->ElementDirectionNow        = PrevElem->ElementDirectionStepBefore;
		
		SnakeElements.Add(NewSankeElem);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector = FVector(0.f, 0.f, 0.f);
	float MovementOffset = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementOffset;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementOffset;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementOffset;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementOffset;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();

	// change directions of head
	SnakeElements[0]->ElementDirectionStepBefore = SnakeElements[0]->ElementDirectionNow;
	SnakeElements[0]->ElementDirectionNow        = LastMoveDirection;

	// move elements
	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElem = SnakeElements[i];
		auto PrevElem = SnakeElements[i - 1];

		FVector PrevLocation = PrevElem->GetActorLocation();
		CurrentElem->SetActorLocation(PrevLocation);

		// change directions of elem
		CurrentElem->ElementDirectionStepBefore = CurrentElem->ElementDirectionNow;
		CurrentElem->ElementDirectionNow        = PrevElem->ElementDirectionNow;
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor* Other)
{
	if (IsValid(OverlappedBlock))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedBlock, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* IntInterface = Cast<IInteractable>(Other);
		if (IntInterface) {
			IntInterface->Interact(this, bIsFirst);
		}
	}
}

